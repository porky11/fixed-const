use num_traits::{int::PrimInt, FromPrimitive, Num, One, ToPrimitive, Zero};

use std::{
    cmp::Ordering,
    num::ParseIntError,
    ops::{Add, Div, Mul, Neg, Rem, Sub},
};

pub trait FixedBase: PrimInt + Num<FromStrRadixErr = ParseIntError> + FromPrimitive {
    type Promoted: PrimInt;

    fn promote(self) -> Self::Promoted;
    fn unpromote(promoted: Self::Promoted) -> Self;
}

macro_rules! impl_fixed_base {
    ($t: ty, $p: ty) => {
        impl FixedBase for $t {
            type Promoted = $p;

            fn promote(self) -> Self::Promoted {
                self as $p
            }

            fn unpromote(promoted: Self::Promoted) -> Self {
                promoted as $t
            }
        }
    };
}

impl_fixed_base!(u8, u16);
impl_fixed_base!(u16, u32);
impl_fixed_base!(u32, u64);
impl_fixed_base!(u64, u128);
impl_fixed_base!(i8, i16);
impl_fixed_base!(i16, i32);
impl_fixed_base!(i32, i64);
impl_fixed_base!(i64, i128);

#[derive(Copy, Clone)]
pub struct Fixed<Base: FixedBase, const FRACTION: u8>(Base);

impl<Base: FixedBase, const FRACTION: u8> Add for Fixed<Base, FRACTION> {
    type Output = Self;

    #[inline]
    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

impl<Base: FixedBase, const FRACTION: u8> Sub for Fixed<Base, FRACTION> {
    type Output = Self;

    #[inline]
    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0)
    }
}

impl<Base: FixedBase, const FRACTION: u8> Mul for Fixed<Base, FRACTION> {
    type Output = Self;

    #[inline]
    fn mul(self, other: Self) -> Self {
        let a = self.0.promote();
        let b = other.0.promote();
        Self(Base::unpromote((a * b) >> FRACTION as usize))
    }
}

impl<Base: FixedBase, const FRACTION: u8> Div for Fixed<Base, FRACTION> {
    type Output = Self;

    #[inline]
    fn div(self, other: Self) -> Self {
        let a = self.0.promote();
        let b = other.0.promote();
        Self(Base::unpromote((a << FRACTION as usize) / b))
    }
}

impl<Base: FixedBase, const FRACTION: u8> Rem for Fixed<Base, FRACTION> {
    type Output = Self;

    #[inline]
    fn rem(self, other: Self) -> Self {
        Self(self.0 % other.0)
    }
}

impl<Base: FixedBase + Neg<Output = Base>, const FRACTION: u8> Neg for Fixed<Base, FRACTION> {
    type Output = Self;

    #[inline]
    fn neg(self) -> Self {
        Self(-self.0)
    }
}

impl<Base: FixedBase, const FRACTION: u8> PartialEq for Fixed<Base, FRACTION> {
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl<Base: FixedBase, const FRACTION: u8> Eq for Fixed<Base, FRACTION> {}

impl<Base: FixedBase, const FRACTION: u8> PartialOrd for Fixed<Base, FRACTION> {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<Base: FixedBase, const FRACTION: u8> Ord for Fixed<Base, FRACTION> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

impl<Base: FixedBase, const FRACTION: u8> Zero for Fixed<Base, FRACTION> {
    #[inline]
    fn zero() -> Self {
        Self(Base::zero())
    }

    #[inline]
    fn is_zero(&self) -> bool {
        self.0.is_zero()
    }
}

impl<Base: FixedBase, const FRACTION: u8> One for Fixed<Base, FRACTION> {
    #[inline]
    fn one() -> Self {
        Self(Base::one() << FRACTION as usize)
    }
}

impl<Base: FixedBase, const FRACTION: u8> ToPrimitive for Fixed<Base, FRACTION> {
    #[inline]
    fn to_usize(&self) -> Option<usize> {
        (self.0 >> FRACTION as usize).to_usize()
    }

    #[inline]
    fn to_u8(&self) -> Option<u8> {
        (self.0 >> FRACTION as usize).to_u8()
    }

    #[inline]
    fn to_u16(&self) -> Option<u16> {
        (self.0 >> FRACTION as usize).to_u16()
    }

    #[inline]
    fn to_u32(&self) -> Option<u32> {
        (self.0 >> FRACTION as usize).to_u32()
    }

    #[inline]
    fn to_u64(&self) -> Option<u64> {
        (self.0 >> FRACTION as usize).to_u64()
    }

    #[inline]
    fn to_u128(&self) -> Option<u128> {
        (self.0 >> FRACTION as usize).to_u128()
    }

    #[inline]
    fn to_isize(&self) -> Option<isize> {
        (self.0 >> FRACTION as usize).to_isize()
    }

    #[inline]
    fn to_i8(&self) -> Option<i8> {
        (self.0 >> FRACTION as usize).to_i8()
    }

    #[inline]
    fn to_i16(&self) -> Option<i16> {
        (self.0 >> FRACTION as usize).to_i16()
    }

    #[inline]
    fn to_i32(&self) -> Option<i32> {
        (self.0 >> FRACTION as usize).to_i32()
    }

    #[inline]
    fn to_i64(&self) -> Option<i64> {
        (self.0 >> FRACTION as usize).to_i64()
    }

    #[inline]
    fn to_i128(&self) -> Option<i128> {
        (self.0 >> FRACTION as usize).to_i128()
    }

    #[inline]
    fn to_f32(&self) -> Option<f32> {
        Some(self.0.to_f32()? / ((1 << FRACTION) as f32))
    }

    #[inline]
    fn to_f64(&self) -> Option<f64> {
        Some(self.0.to_f64()? / ((1 << FRACTION) as f64))
    }
}

impl<Base: FixedBase, const FRACTION: u8> FromPrimitive for Fixed<Base, FRACTION> {
    #[inline]
    fn from_usize(n: usize) -> Option<Self> {
        Some(Self(Base::from_usize(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_u8(n: u8) -> Option<Self> {
        Some(Self(Base::from_u8(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_u16(n: u16) -> Option<Self> {
        Some(Self(Base::from_u16(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_u32(n: u32) -> Option<Self> {
        Some(Self(Base::from_u32(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_u64(n: u64) -> Option<Self> {
        Some(Self(Base::from_u64(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_u128(n: u128) -> Option<Self> {
        Some(Self(Base::from_u128(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_isize(n: isize) -> Option<Self> {
        Some(Self(Base::from_isize(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_i8(n: i8) -> Option<Self> {
        Some(Self(Base::from_i8(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_i16(n: i16) -> Option<Self> {
        Some(Self(Base::from_i16(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_i32(n: i32) -> Option<Self> {
        Some(Self(Base::from_i32(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_i64(n: i64) -> Option<Self> {
        Some(Self(Base::from_i64(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_i128(n: i128) -> Option<Self> {
        Some(Self(Base::from_i128(n)? << FRACTION as usize))
    }

    #[inline]
    fn from_f32(n: f32) -> Option<Self> {
        Some(Self(Base::from_f32(n * ((1 << FRACTION) as f32))?))
    }

    #[inline]
    fn from_f64(n: f64) -> Option<Self> {
        Some(Self(Base::from_f64(n * ((1 << FRACTION) as f64))?))
    }
}
